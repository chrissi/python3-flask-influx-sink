#!/usr/bin/env python3

from flask import Flask
from flask import Response
app = Flask(__name__)

import json
import yaml
import hashlib
import os

from influxdb import InfluxDBClient

def checkMac(value, mac, params):
    h = hashlib.sha256()
    h.update("{}|{}".format(value, params["key"]).encode())
    localHash = ".".join([str(x) for x in h.digest()])
    if mac == localHash:
        return True
    else:
        return False

def writeToDb(value, params, config):
    # connect to influxdb
    ic = InfluxDBClient(config["influx"]["server"], config["influx"]["port"], config["influx"]["user"], config["influx"]["pass"], config["influx"]["db"])
    payload = []
    payload.append({\
                    "measurement": params["measurement"], \
                    "tags": params["tags"], \
                    "fields": {\
                                params["measurement"]: value\
                    }\
    })
    ic.write_points(payload)

def get_response(state, text):
    d = {
        "success": state,
        "message": text
    }
    return Response(json.dumps(d), mimetype="application/json")

@app.route("/write/<sensor_name>/<value>/<mac>")
def write(sensor_name, value, mac):
    #load configuration
    p = os.path.dirname(os.path.realpath(__file__))
    p = os.path.join(p, "conf.yaml")
    with open(p, "r") as fh:
        config = yaml.load(fh)

    if sensor_name in config["sensors"]:
        params = config["sensors"][sensor_name]
        if checkMac(value, mac, params) == True:
            try:
                writeToDb(value, params, config)
            except Exception as e:
                return get_response(False, "DB connection falied: {}".format(e))
            return get_response(True, "Written to db.")
        else:
            return get_response(False, "MAC missmatch")
    else:
        return get_response(False, "SensorName unknown")

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=8001)
